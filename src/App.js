import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Card from './Card';
import Table from './Table';

class App extends Component {

  constructor(props){
    super();
    this.state={
      data:[],
      tableData:[]
    }
  }

  componentDidMount(){
  fetch('https://jsonplaceholder.typicode.com/photos/')
  // We get the API response and receive data in JSON format...
  .then(response => response.json())
  // ...then we update the users state
  .then(data =>
    this.setState({
      data: data
    })
  )
  // Catch any errors we hit and update the app
  .catch(error => console.log(error));
}

addToCompare = (data)=>{
  var rows = [...this.state.tableData];
      rows.push(data);
      this.setState({tableData:rows});
}

removeFromCompare = (data)=>{
  var newRows = [...this.state.tableData];
  newRows.splice(newRows.indexOf(data), 1);
  this.setState({tableData:newRows});
}

  render() {
    var content = [];
    if(this.state.data){
      content = this.state.data;
    }
    return (
      <div className="App">

      <div className="col-md-12" style={{height: "450px",overflow: "scroll"}}>
      <div className="card-deck">
      {content.length > 0 ? content.map((c)=><Card data={c} key={c.id} tableData={this.state.tableData} addToCompare={this.addToCompare} removeFromCompare={this.removeFromCompare}/>) :<p className="loading">Loading....</p>}

     </div>
     </div>
     <br/>
     <Table tableData={this.state.tableData}/>
      </div>
    );
  }
}

export default App;
