import React from 'react';

function Table(props){
  return (
    <div>
      <p scope="row">COMPARISON TABLE</p>
      {props.tableData.length > 0 ?
    <table className="table">

       <thead>

         <tr>
           <th scope="col"></th>
           <th scope="col">Id</th>
           <th scope="col">URL</th>
           <th scope="col">Title</th>
         </tr>
       </thead>
       <tbody>
       {props.tableData.map((row)=>(
         <tr key={row.id}>
           <td> <img className="small-img" src={row.url} /></td>
           <td>{row.id}</td>
           <td>{row.url}</td>
           <td>{row.title}</td>
         </tr>
       ))}

       </tbody>
     </table> :
     <p>Please Add data for Comparison</p>}
     </div>
  )
}


export default Table;
