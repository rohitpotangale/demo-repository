import React from 'react';

function Card(props) {
    var button = <button className="btn btn-primary" onClick={ () => props.addToCompare(props.data)}>Compare</button>;

    button = props.tableData.includes(props.data) ? <button className="btn btn-danger" onClick={ () => props.removeFromCompare(props.data)}>Remove</button> : <button className="btn btn-primary" onClick={ () => props.addToCompare(props.data)}>Compare</button>;


  return (
    <div className="col-md-3" style={{marginBottom:"20px"}}>
    <div className="card">
      <img className="card-img-top" src={props.data.url} alt="Card image cap" />
      <div className="card-body">
        <h5 className="card-title">{props.data.title}</h5>
        <p className="card-text">{props.data.id}</p>
        <p className="card-text">{props.data.url}</p>
      </div>
      <div className="card-footer">
        {button}
      </div>
    </div>
    </div>
  )
}


export default Card;
